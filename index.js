$(document).ready(() => {
  const TODO_PER_PAGE = 5;
  const ENTER_KEY = 13;
  const $pagination = $('.pagination');

  let currentPage = 1;
  let todoArray = [];
  let filterType = 'AllTodos';

  const charReplace = function(name) {
    const eMap = {
      '"': '&quot;',
      '&': '&amp;',
      '\'': '&#39;',
      '/': '&#x2F;',
      '<': '&lt;',
      '=': '&#x3D;',
      '>': '&gt;',
      '`': '&#x60;',
    };

    return name.replace(/[&<>"'`=/]/gu, el => eMap[el]);
  };

  const filtration = () => {
    let filtrateArray = [];

    if (filterType === 'AllTodos') {
      filtrateArray = todoArray;
    } else if (filterType === 'ShowActiveTodos') {
      filtrateArray = todoArray.filter(item => item.done === false);
    } else if (filterType === 'ShowCompletedTodos') {
      filtrateArray = todoArray.filter(item => item.done === true);
    }

    return filtrateArray;
  };

  const addPages = () => {
    $pagination.empty();
    const numberOfPages = Math.ceil(filtration().length / TODO_PER_PAGE);

    let pages = '';


    for (let i = 1; i <= numberOfPages; i++) {
      pages += `<button class="button-page"
id="${i}" value="${i}" id="page">${i}</button>`;
    }
    $pagination.append(pages);
  };

  const slicingPerPages = function() {
    const sortFilter = filtration();
    const allPossibleTodo = currentPage * TODO_PER_PAGE;
    const firstTodoPage = allPossibleTodo - TODO_PER_PAGE;
    const lastTodoPage = firstTodoPage + TODO_PER_PAGE;

    return sortFilter.slice(firstTodoPage, lastTodoPage);
  };

  const render = function() {
    $('#todo-roster').empty();
    const sliceArray = slicingPerPages();

    let stringForAppend = '';

    sliceArray.forEach(currentTodo => {
      let todoItemClass = '';

      if (currentTodo.done) {
        todoItemClass = 'todo-item line';
      } else {
        todoItemClass = 'todo-item';
      }

      stringForAppend += `<li id=${currentTodo.id}
class='${todoItemClass}' class="todo-item">
        <div> <input type="checkbox" class="todo-item-checkbox"
${currentTodo.done && 'checked'} /></div>
      <span class="todo-item-value">${currentTodo.value}</span>
        <div><button class="delete-button">Delete</button></div>
          </li>`;
    });

    $('#todo-roster').append(stringForAppend);

    const { length: active } = todoArray.filter(item => item.done === false);
    const { length: complete } = todoArray.filter(item => item.done === true);

    const condition = todoArray.every(item => item.done === true)
      && todoArray.length;

    $('#check-all')
      .prop('checked', condition);

    if (todoArray.length > 0) {
      $('#todo-roster')
        .append(`<hr><p>Active todo: ${active} 
                                 Completed todo: ${complete}</p>`);
    }
    addPages();
  };

  $pagination.on('click', 'button', event => {
    currentPage = $(event.currentTarget).attr('value');
    render();
    $(`#${currentPage}.button-page`).addClass('button-select');
  });

  $('#add-button').click(() => {
    const newValue = $('#todo-input')
      .val()
      .trim();

    const replacedValue = charReplace(newValue);

    if (newValue !== '') {
      const newId = new Date().getTime();
      const newTodoObj = {
        done: false,
        id: newId,
        value: replacedValue,
      };

      todoArray.push(newTodoObj);
      $('#todo-input')
        .val('');
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $(`#${currentPage}.button-page`).addClass('button-select');
    }
  });

  $('#todo-input')
    .focus();

  $('#todo-input').on('keyup', event => {
    if (event.which === ENTER_KEY) {
      $('#add-button')
        .click();
    }
  });

  $(document)
    .on('change', '#check-all', function() {
      todoArray.forEach(el => {
        if (this.checked) {
          el.done = true;
        } else {
          el.done = false;
        }
        currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      });
      render();
      $(`#${currentPage}.button-page`).addClass('button-select');
    });

  $(document)
    .on('change', '.todo-item-checkbox', function() {
      todoArray.forEach(el => {
        if (el.id === Number(this.parentElement.parentElement.id)) {
          el.done = !el.done;
        }
      });
      render();
      $(`#${currentPage}.button-page`).addClass('button-select');
    });

  $(document).on('click', '.delete-button', function() {
    todoArray.forEach((el, index) => {
      if (el.id === Number(this.parentElement.parentElement.id)) {
        todoArray.splice(index, 1);
      }

      const isLastPage = currentPage
        >= Math.ceil(filtration().length / TODO_PER_PAGE);
      const shouldRemove = filtration().length % TODO_PER_PAGE === 0;

      if (isLastPage && shouldRemove) {
        currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      }
    });
    render();
    $(`#${currentPage}.button-page`).addClass('button-select');
  });

  $(document)
    .on('click', '#all', () => {
      filterType = 'AllTodos';
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $('#active').removeClass('select-tab');
      $('#complete').removeClass('select-tab');
      $('#all').addClass('select-tab');
      $(`#${currentPage}.button-page`).addClass('button-select');
    });

  $(document)
    .on('click', '#complete', () => {
      filterType = 'ShowCompletedTodos';
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $('#active').removeClass('select-tab');
      $('#all').removeClass('select-tab');
      $('#complete').addClass('select-tab');
      $(`#${currentPage}.button-page`).addClass('button-select');
    });

  $(document)
    .on('click', '#active', () => {
      filterType = 'ShowActiveTodos';
      currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      render();
      $('#complete').removeClass('select-tab');
      $('#all').removeClass('select-tab');
      $('#active').addClass('select-tab');
      $(`#${currentPage}.button-page`).addClass('button-select');
    });


  $('#todo-roster')
    .on('dblclick', 'span', event => {
      const title = $(event.currentTarget).text();
      const $thisParent = $(event.currentTarget).parent();

      $thisParent.html(`<input type="text" id="edit" value="">`);
      $thisParent.removeClass('line');

      const $edit = $('#edit');

      $edit.focus();
      $edit.val(title);
    });


  const saveEdit = () => {
    const clickId = Number($(event.target)
      .parent()
      .attr('id'));
    const value = $('#edit')
      .val()
      .trim();

    const replacedValue = charReplace(value);

    if (!value) {
      render();

      return false;
    }

    todoArray.forEach(item => {
      if (item.id === clickId) {
        item.value = replacedValue;
      }
      render();
    });

    return todoArray;
  };

  $('#todo-roster')
    .on('keydown', '#edit', event => {
      if (event.which === ENTER_KEY) {
        saveEdit();
      }
    });
  $('#todo-roster')
    .on('blur', '#edit', saveEdit);

  $(document)
    .on('click', '#delete-all', () => {
      todoArray = todoArray.filter(el => el.done === false);

      const isPageOver = currentPage
        > Math.ceil(filtration().length / TODO_PER_PAGE);

      if (isPageOver) {
        currentPage = Math.ceil(filtration().length / TODO_PER_PAGE);
      }
      render();
      $(`#${currentPage}.button-page`).addClass('button-select');
    });
});
